/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package xo.xoweek2;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class XOWeek2 {
    
    private static char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private static char turn = 'O';
    private static int row;
    private static Scanner sc = new Scanner(System.in);
    private static int col;
    private static int count = 0;

    public static void main(String[] args) {
        printWelcome();
        while(true) {
            showTable();
            showTurn();
            inputRowCol();
            if(isFinish()) {
                showTable();
                showResult();
                break;
            }
            switchTurn();
        }
        showTable();
         
    }

    private static void printWelcome() { //Welcome to XO Game 
        System.out.println("Welcome to XO Game !!");
    }
    private static void showTable() { //Create XO Table
        for(int r = 0; r < 3; r++) {
            for(int c = 0; c < 3; c++) {
                System.out.print(table [r][c] + " ");
            }
            System.out.println();
        }
    }
    private static void showTurn() { //Show X,O Turn
        System.out.println("Turn " + turn);
    }
    private static void inputRowCol() {
    while (true) {
        System.out.print("Input row, col kub : ");
        row = sc.nextInt() - 1;
        col = sc.nextInt() - 1;
        if (row < 0 || row > 2 || col < 0 || col > 2) {
            System.out.println("Invalid input. Please enter again.");
            continue;
        }

        if (table[row][col] != '-') {
            System.out.println("This cell has already put in. Try another cell");
            continue;
        }
        table[row][col] = turn;
        count++;
        break;
    }

    }
    private static void switchTurn() { //Switch Turn
        if (turn == 'X') {
            turn = 'O';
        } else {
            turn = 'X';
        }
    }
    private static boolean isFinish() {
        if(checkWin()) {
            return true;
        }
        if(checkDraw()) {
            return true;
        }
        return false;
    }
    private static boolean checkWin() {
        if(checkRow()) {
            return true;
        }
        if(checkCol()) {
            return true;
        }
        if(checkX()) {
            return true;
        }
        return false;
    }
    
    private static boolean checkDraw() {
        if(count==9) {
            return true;
        }
        return false;
    }
    
    private static boolean checkRow() {
        if(table[row][0] == turn && table[row][1] == turn && table[row][2] == turn) {
            return true;
        }
        return false;
    }

    private static boolean checkCol() {
        if(table[0][col] == turn && table[1][col] == turn && table[2][col] == turn) {
            return true;
        }
        return false;  
    }

    private static boolean checkX() {
        if(checkX1()) {
            return true;
        }
        if(checkX2()) {
            return true;
        }
        return false;  
    }
    private static boolean checkX2() {
        return false; 
    }

    private static boolean checkX1() {
        return false; 
    }

    private static void showResult() {
        if(checkWin()) {
            System.out.println(turn + " Win !");
        }
        if(checkDraw()) {
            System.out.println("Draw !");
        }
    }
}